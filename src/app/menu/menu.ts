import {Component, HostListener, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {Platform} from '@ionic/angular';


@Component({
    selector: 'app-menu',
    templateUrl: './menu.html',
    styleUrls: ['./menu.scss'],
})
export class MenuComponent implements OnInit {

    public menuPages = [
        {
            name: 'Home',
            url: '/',
            hide: 0
        },
        {
            name: 'Consumer Industrial',
            url: 'topic/ConsumerIndustrial',
            hide: 0
        },
        {
            name: 'Real Estate',
            url: 'topic/RealEstate',
            hide: 0
        },
        {
            name: 'Law Financial',
            url: 'topic/LawFinancial',
            hide: 0
        },
        {
            name: 'Technology',
            url: 'topic/Technology',
            hide: 0
        },
        {
            name: 'Universities',
            url: 'topic/Academia',
            hide: 0
        },
        {
            name: 'Human Resources',
            url: 'topic/HumanResources',
            hide: 0
        },
        {
            name: 'Services',
            url: 'topic/Services',
            hide: 0
        },
        {
            name: 'Marketing',
            url: 'topic/Marketing',
            hide: 0
        },
        {
            name: 'HealthCare',
            url: 'topic/HealthCare',
            hide: 0
        },
        {
            name: 'New Posts',
            url: '/admin/posts/add',
            hide: 0
        },
        {
            name: 'View Posts',
            url: '/admin/posts/home',
            hide: 0
        }
    ];
    public selectedIndex = 0;
    public i: number;

    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        private router: Router,
    ) {
        this.initializeApp();

    }

    @HostListener('window:beforeunload', ['$event'])
    beforeUnloadHandler(event: any) {
        // this.userService.Logout();
    }

    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleDefault();
            this.splashScreen.hide();
        });
    }

    ngOnInit() {
        //
    }
}
