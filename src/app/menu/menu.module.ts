import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MenuComponent} from './menu';
import {FormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';
import {RouterModule} from '@angular/router';

@NgModule({
  declarations: [MenuComponent],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule
  ],
  exports: [MenuComponent],
  entryComponents: [MenuComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]

})
export class MenuComponentModule {}
