import {Component, OnInit} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ActivatedRoute} from '@angular/router';
import {Article} from '../dataset/Article';
import {DomSanitizer, Meta, SafeHtml, Title} from '@angular/platform-browser';

// @IonicPage({
//     name: 'welcome'
// })
@Component({
    selector: 'app-article',
    templateUrl: './article.page.html',
    styleUrls: ['./article.page.scss'],
})
export class ArticlePage implements OnInit {
    articleSlug = '';
    articleThread: Article;
    assetUrl = environment.s3BucketBaseUrl;
    localPath = '../../../assets/img/cropped-TRANS-LARGE-1.jpeg';
    htmlSnippet: SafeHtml;

    constructor(
        private route: ActivatedRoute, private http: HttpClient, private domSanitizer: DomSanitizer,
        public meta: Meta, public title: Title
    ) {
    }


    ngOnInit() {
        this.articleSlug=this.route.snapshot.params.articleSlug;

        this.http
            .get<Article>(
                environment.apiEndPoint + 'thread/slug/' + this.articleSlug,
                {
                    headers: new HttpHeaders(
                        {
                            'Content-Type': 'application/json'
                        })
                }
            )
            .subscribe(articleThread => {
                    this.htmlSnippet = this.domSanitizer.bypassSecurityTrustHtml(articleThread[0].body);
                    this.articleThread = articleThread;
                    this.title.setTitle(this.articleThread[0].title);
                    const url = location.origin + '/threads/' + this.articleSlug;
                    this.meta.updateTag({name: 'description', content: this.articleThread[0].intro});
                    this.meta.updateTag({name: 'title', content: this.articleThread[0].title});
                    // this.meta.updateTag({name: 'image', content: this.articleThread[0].awsUrl});
                    // this.meta.updateTag({name: 'image', content: this.articleThread[0].awsUrl});
                    // this.meta.updateTag({property: 'name', content: this.articleThread[0].title});
                    // this.meta.updateTag({property: 'description', content: this.articleThread[0].intro});
                    this.meta.updateTag({property: 'og:name', content: this.articleThread[0].title});
                    this.meta.updateTag({property: 'og:image', content: this.articleThread[0].awsUrl});
                    this.meta.updateTag({property: 'og:image:secure', content: this.articleThread[0].awsUrl});
                    this.meta.updateTag({property: 'og:url', content: url});
                    this.meta.updateTag({property: 'og:type', content: 'website'});
                    this.meta.updateTag({property: 'og:title', content: this.articleThread[0].title});
                    this.meta.updateTag({property: 'og:site_name', content: this.articleThread[0].title});
                    this.meta.updateTag({property: 'og:description', content: this.articleThread[0].intro});
                    this.meta.updateTag({property: 'twitter:card', content: 'summary_large_image'});
                    this.meta.updateTag({property: 'twitter:title', content: this.articleThread[0].title});
                    this.meta.updateTag({property: 'twitter:url', content: url});
                    this.meta.updateTag({property: 'twitter:description', content: this.articleThread[0].intro});
                    this.meta.updateTag({property: 'twitter:image:src', content: this.articleThread[0].awsUrl});
                    this.meta.updateTag({property: 'twitter:creator', content: `yorkshire business journal`});
                    this.meta.updateTag({property: 'twitter:domain', content: `www.yorkshirebusinessjournal.co.uk`});
                    console.log({property: 'og:image:secure', content: this.articleThread[0].awsUrl});
                    /**
                     *    <meta name="description" content="A weekly business news website" >
                     *     <meta name="title" content="YBJ">
                     *     <!-- Schema.org for Google -->
                     *     <meta property="og:image" content="https://metatags.io/assets/meta-tags-16a33a6a8531e519cc0936fbba0ad904e52d35f34a46c97a2c9f6f7dd7d336f2.png">
                     *     <meta property="og:image:width" content="600" >
                     *     <meta property="og:image:height" content="314" >
                     *     <meta name="twitter:card" content="">
                     *     <meta name="twitter:url" content="">
                     *     <meta name="twitter:title" content="">
                     *     <meta name="twitter:description" content="">
                     *     <meta name="twitter:image:src" content="">
                     */
                },
                error => console.log(error)
            );

    }

}
