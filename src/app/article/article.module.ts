import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ArticlePageRoutingModule } from './article-routing.module';

import { ArticlePage } from './article.page';
import {HeaderTitlePageModule} from '../headerTitle/header-title.module';
import {ShareButtonsModule} from 'ngx-sharebuttons/buttons';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ArticlePageRoutingModule,
        HeaderTitlePageModule,
        ShareButtonsModule
    ],
  declarations: [ArticlePage]
})
export class ArticlePageModule {}
