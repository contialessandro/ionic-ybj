import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';
import {QuillModule} from 'ngx-quill';
import * as Quill from 'quill';

class PreserveWhiteSpace {
    constructor(private quill: any) {
        quill.container.style.whiteSpace = 'pre-line';
    }
}

Quill.register('modules/preserveWhiteSpace', PreserveWhiteSpace);

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule
    ],
    declarations: [],
    exports: [QuillModule],
})
export abstract class AbstractQuillModule {
}