import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {AuthGuard} from './services/auth.guard';
import {HomePage} from "./admin/post/home/home.page";

const routes: Routes = [
    {
        path: '',
        loadChildren: () => import('./home/home.module').then(m => m.HomePageModule),
        pathMatch: 'full'
    }, {
        path: 'login',
        loadChildren: () => import('./login/login.module').then(m => m.LoginPageModule)
    },
    {
        path: 'admin/posts/home',
        //loadChildren: () => import('./view/view.module').then( m => m.ViewPageModule)
        component: HomePage,
        canActivate: [AuthGuard]
    },
    {
        path: 'admin/posts/add',
        loadChildren: () => import('./admin/post/create-post/create-post.module').then(m => m.CreatePostPageModule),
        canActivate: [AuthGuard]
    },
    {
        path: 'admin/posts/edit/:id',
        loadChildren: () => import('./admin/post/edit-post/edit-post.module').then(m => m.EditPostPageModule),
        canActivate: [AuthGuard]
    },
    {
        path: 'home',
        loadChildren: () => import('./home/home.module').then(m => m.HomePageModule)
    },
    {
        path: 'threads/:articleSlug',
        loadChildren: () => import('./article/article.module').then(m => m.ArticlePageModule)
    },
    {
        path: 'topic/:category',
        loadChildren: () => import('./category/category.module').then(m => m.CategoryPageModule)
    },

];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules}),
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
