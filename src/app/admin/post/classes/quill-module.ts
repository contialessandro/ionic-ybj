import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';
import {AbstractQuillModule} from '../../../Classes/quill-module';
import {QuillModule} from 'ngx-quill';

const quillToolbarOptions = [
    [{font: []}, {size: []}],
    ['bold', 'italic', 'underline'],
    [{color: []}, {background: []}],
    [{list: 'ordered'}, {list: 'bullet'}],
    [{align: []}],
    [{header: [1, 2, 3, 4, 5, 6, false]}],
];


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        QuillModule.forRoot({
            modules: {
                toolbar: quillToolbarOptions,
                preserveWhiteSpace: true,
            }
        })
    ],
    exports: [QuillModule],
    declarations: []
})
export class CustomQuillModule extends AbstractQuillModule {
}