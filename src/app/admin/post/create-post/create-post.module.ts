import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreatePostPageRoutingModule } from './create-post-routing.module';

import { CreatePostPage } from './create-post.page';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        CreatePostPageRoutingModule,
        CKEditorModule,
        ReactiveFormsModule
    ],
    exports: [
        CreatePostPage
    ],
    declarations: [CreatePostPage]
})
export class CreatePostPageModule {
}
