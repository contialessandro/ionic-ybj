import { Component, OnInit, ViewChild } from '@angular/core';
import { IonDatetime, MenuController, Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { Locations } from '../../../dataset/Locations';
import { Categories } from '../../../dataset/CategoryArticle';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Article } from '../../../dataset/Article';
import { Router } from '@angular/router';

@Component({
    selector: 'app-create-post',
    templateUrl: './create-post.page.html',
    styleUrls: ['./create-post.page.scss'],
})
export class CreatePostPage implements OnInit {

    activePageTitle = 'Dashboard';

    activeIndex: number;
    public Editor = ClassicEditor;
    public locations: [Locations];
    public categories: [Categories];
    @ViewChild(IonDatetime, {static: true}) datetime: IonDatetime;
    categoryEndpoint = environment.apiEndPoint + 'categories/News';
    locationsEndpoint = environment.apiEndPoint + 'locations';
    apiURL = environment.apiEndPoint;
    ionicForm: any;
    file: any;
    fileStoragePath: any;
    slug: string;
    nextPublishDate: any;

    constructor(
        public platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        private http: HttpClient,
        private menuCtrl: MenuController,
        private router: Router
    ) {
        this.initializeApp();
        if (this.platform.is('desktop')) {
            // This will only print when on desktop
        }
    }

    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleDefault();
            this.splashScreen.hide();
        });
    }

    ngOnInit() {
        this.LoadLocations();
        this.LoadCategories();
        this.LoadNextPublishDate();
        this.formInit();
    }

    LoadLocations() {
        return this.http
            .get<[Locations]>(
                `${this.locationsEndpoint}/`,
                {
                    headers: new HttpHeaders(
                        {
                            'Content-Type': 'application/json',
                        })
                }
            )
            .subscribe(locations => {
                    this.locations=locations;
                },
                error => console.log(error)
            );
    }

    LoadCategories() {
        const api=`${this.categoryEndpoint}/`;
        return this.http.get<[Categories]>(api).subscribe(categories => {
                this.categories=categories;
            },
            error => console.log(error)
        );

    }

    LoadNextPublishDate() {
        const api=`${this.apiURL}/admin/next_publish_date`;
        return this.http.get<any>(api).subscribe(string => {
                this.nextPublishDate = string;
            },
            error => {
                if (error.status === 401) {
                    console.log(error.error);
                    this.router.navigate(['login']);
                }
            }
        );
    }

    formInit() {
        this.ionicForm=new FormGroup({
            title: new FormControl(null, [Validators.required]),
            intro: new FormControl(null, [Validators.required]),
            url: new FormControl(null, [Validators.required]),
            editor: new FormControl(null, [Validators.required]),
            hidden: new FormControl(null, [Validators.required]),
            location: new FormControl('jydCrY6TVqBud9bRKZGg3C', [Validators.required]),
            publish: new FormControl(this.nextPublishDate, [Validators.required]),
            category: new FormControl('621264b5efd8794564aa8db6', [Validators.required]),
            image: new FormControl(null, [Validators.required])
        });
    }


    async submitForm() {
        const payload: any={
            thread: {
                title: this.ionicForm.value.title,
                intro: this.ionicForm.value.intro,
                body: this.ionicForm.value.editor,
                published_at: this.ionicForm.value.publish,
                hide: this.ionicForm.value.hidden,
                url: this.ionicForm.value.url,
                slug: 'asdad'
                /*this.ionicForm.value.title.replace(/\s/g, '-').toLowerCase()*/
            },
            thread_meta: {
                location_id: this.ionicForm.value.location,
                category_id: this.ionicForm.value.category,
                user_id: 1,
            },
            image_meta: {
                file_object: '', file_storage_path: ''
            }
        };
        this.slug=this.ionicForm.value.title.toLowerCase().replace(/\s/g, '-');
        const res: any=await this.SaveFile();
        await this.SavePost();
        // console.log(this.file_storage_path);
        const url=this.apiURL + '/admin/file/upload';

        payload.file_object=this.file;
        payload.image_meta.file_storage_path=this.fileStoragePath;

        this.http.put(url, payload).subscribe((response) => {
        });
        this.router.navigate([`posts/view`]);
    }

    onFileChange(fileChangeEvent) {
        this.file=fileChangeEvent.target.files[0];
    }

    SaveFile=async () => {
        (await this.GetCampaignSignedUrl({type: this.slug, file_name: this.file.name, file_type: this.file.type})
        )
            .subscribe(async (a: {
                signed_url: any, file_storage_path: any
            }) => {
                await this.UploadFile(a.signed_url);
                this.fileStoragePath=a.file_storage_path;
                const route=this.apiURL + `/admin/file/signedUrl`;
                return this.http.put(a.signed_url, this.file, {headers: {'Content-Type': this.file.type}});
            });
    }

    UploadFile=async (signedUrl: string) => {
        return this.http.put(signedUrl, this.file, {headers: {'Content-Type': this.file.type}}).subscribe(a => {
        });
    }

    GetCampaignSignedUrl=async (payload: any) => {
        const route=this.apiURL + `/admin/file/signedUrl`;
        return this.http.post<any>(route, payload);
    }

    SavePost=async () => {
        const publishTimestamp=`${this.ionicForm.value.publish}`;
        const dt=new Date(publishTimestamp);

        const post: Article={
            image: this.file.name,
            alt: this.file.name,
            body: this.ionicForm.value.editor,
            category: this.ionicForm.value.category,
            color: '#ffeb00de',
            created_at: `${this.ionicForm.value.publish} 00:00:00`,
            published_at: dt,
            filename: this.file.name,
            id: 0,
            intro: this.ionicForm.value.intro,
            location: this.ionicForm.value.location,
            media_id: '0',
            slug: this.slug,
            hide: Boolean(+this.ionicForm.value.hidden),
            ticker: '...',
            title: this.ionicForm.value.title,
            url: this.ionicForm.value.url,
            awsUrl: `${environment.s3BucketBaseUrl}/${this.fileStoragePath}`
        };
        const route=this.apiURL + `/admin/posts`;
        return this.http.post<any>(route, post).subscribe(a => {
        }, error => {
            console.log(error);
        });
    }

    openMenu() {
        this.menuCtrl.enable(true, 'menuA')
            .then(myMenu => {
                // console.log(myMenu);
                this.menuCtrl.open('menuA');
            });
    }
}
