import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CreatePostPage } from './create-post.page';
import { AuthGuard } from '../../../services/auth.guard';

const routes: Routes=[
    {
        path: '',
        component: CreatePostPage,
        canActivate: [AuthGuard]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class CreatePostPageRoutingModule {
}
