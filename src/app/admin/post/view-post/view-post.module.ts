import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';


import {ViewPostPage} from './view-post.page';
import {QuillModule} from "ngx-quill";
import {HTTP_INTERCEPTORS} from "@angular/common/http";
import {AuthInterceptor} from "../../../services/authconfig.interceptor";
import {NgxDatatableModule} from "@swimlane/ngx-datatable";
import {NgxPaginationModule} from "ngx-pagination";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        NgxDatatableModule,
        QuillModule.forRoot({
            modules: {
                syntax: true
            }
        }),
        NgxPaginationModule,
    ],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthInterceptor,
            multi: true
        },
    ],
    declarations: [ViewPostPage]
})
export class ViewPostPageModule {
}
