import {Component, OnInit} from '@angular/core';
import {MenuController, Platform} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {HttpClient} from '@angular/common/http';
import {Route, Router} from '@angular/router';
import {CrudService} from '../../../services/crud.service';
import {Article} from '../../../dataset/Article';
import {Observable} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {finalize} from "rxjs/internal/operators/finalize";
import {ToastService} from "../../../services/toast.service";


@Component({
    selector: 'app-view-post',
    templateUrl: './view-post.page.html',
    styleUrls: ['./view-post.page.scss'],
})
export class ViewPostPage implements OnInit {

    dataIsLoaded = false;
    posts;
    cp = 1;
    rows: Article[];
    columns = [
        {prop: 'Title', name: 'Title', width: 100},
        {prop: 'intro', name: 'Intro'},
        {prop: 'slug', name: 'Uri'}
    ];

    constructor(
        public platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        private http: HttpClient,
        private menuCtrl: MenuController,
        private router: Router,
        private crudService: CrudService,
        private toastService: ToastService
        // private ionLoaderService: IonLoaderService
    ) {
        this.initializeApp();
        if (this.platform.is('desktop')) {
            // This will only print when on desktop
        }
    }

    ionViewDidEnter() {
        this.GetPosts().subscribe(posts => {
                this.posts=posts.reverse();
                // this.hideLoader();
                this.dataIsLoaded = true;
            },
            error => console.log(error)
        );
        if (this.dataIsLoaded) {

        }

    }

    redirectTo = (url: string, id: number = undefined) => (id) ? this.router.navigate([url, id]) : this.router.navigate([url]);


    private LoadAllPosts() {
        this.http
            .get<[Article]>(
                environment.apiEndPoint + `posts`)
            .pipe(finalize(() => this.dataIsLoaded = true))
            .subscribe((res) => {
                this.rows = res;

            }, this.toastService.handleError);
    }


    ngOnInit() {
        // this.showLoader();
        this.printpath('', this.router.config);
    }

    printpath(parent: string, config: Route[]) {
        // tslint:disable-next-line:prefer-for-of
        for (let i = 0; i < config.length; i++) {
            const route=config[i];
            if (route.children) {
                const currentPath=route.path ? parent + '/' + route.path : parent;
                this.printpath(currentPath, route.children);
            }
        }
    }

    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleDefault();
            this.splashScreen.hide();
        });
    }

    // @ts-ignore
    GetPosts(): Observable<[Article]> {
        return this.http
            .get<[Article]>(
                environment.apiEndPoint + `posts`);
    }

    isPublished(post: Article): boolean {
        return post.published_at < new Date().toISOString() && !post.hide;
    }

    OpenPost(post) {
        // window.open(`home/posts/edit/${post._id}`, '_self');
        // this.router.navigateByUrl(`${post._id}`);
    }

    // displayAutoLoader() {
    //     this.ionLoaderService.autoLoader();
    // }
    //
    // showLoader() {
    //     this.ionLoaderService.simpleLoader();
    // }
    //
    // hideLoader() {
    //     this.ionLoaderService.dismissLoader();
    // }
    //
    // customizeLoader() {
    //     this.ionLoaderService.customLoader();
    // }
}
