import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {AuthGuard} from '../../services/auth.guard';
import {ViewPostPage} from "./view-post/view-post.page";
import {ViewPostPageModule} from "./view-post/view-post.module";
import {HomePage} from "./home/home.page";

const routes: Routes = [
    {
        path: '',
        //loadChildren: () => import('./view/view.module').then( m => m.ViewPageModule)
        component: HomePage,
        canActivate: [AuthGuard]
    },
    {
        path: 'add',
        loadChildren: () => import('./create-post/create-post.module').then(m => m.CreatePostPageModule),
        canActivate: [AuthGuard]
    },
    {
        path: 'edit/:postId',
        loadChildren: () => import('./edit-post/edit-post.module').then(m => m.EditPostPageModule),
        canActivate: [AuthGuard]
    },
    // {
    //     path: '',
    //     component: ViewPostPage,
    //     canActivate: [AuthGuard],
    //     pathMatch: 'full'
    // }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class PostCrudRoutingModule {
}
