import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MenuController, NavController, Platform} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';
import {Locations} from '../../../dataset/Locations';
import {Categories} from '../../../dataset/CategoryArticle';
import {CrudService} from "../../../services/crud.service";
import {DomSanitizer} from "@angular/platform-browser";
import Quill from 'quill';
import {Observable} from "rxjs";
import {ToastService} from "../../../services/toast.service";
import {Article} from "../../../dataset/Article";
import {environment} from "../../../../environments/environment";

@Component({
    selector: 'app-edit-post',
    templateUrl: './edit-post.page.html',
    styleUrls: ['./edit-post.page.scss'],
})
export class EditPostPage implements OnInit {
    categories: Observable<Categories[]>;
    editor: Quill;
    locations: Observable<Locations[]>;
    ionicForm: FormGroup;
    post: any;
    postId: any;

    constructor(public platform: Platform,
                private splashScreen: SplashScreen,
                private statusBar: StatusBar,
                private http: HttpClient,
                private menuCtrl: MenuController,
                private router: Router,
                private route: ActivatedRoute,
                private crudService: CrudService,
                private navCtrl: NavController,
                private domSanitizer: DomSanitizer,
                private toastService: ToastService) {

    }

    onEditorCreated = (editor: Quill) => {
        this.editor = editor;
    };


    onContentChanged = (event: any) => {
        console.log(this.editor.root.innerHTML);
    }

    ngOnInit() {
        this.createForm();
        this.crudService.getLocations();
        this.crudService.getCategories();
        this.postId = this.route.snapshot.params.postId;
        if (!this.route.snapshot.params.postId) {
            this.navCtrl.navigateBack('posts/view');
            return;
        }
        this.getPost();
        console.log(this.editor.delta);


    }

    onFileChange($event: Event) {

    }


    getPost() {
        console.log(this.postId);
        this.http
            .get<[Article]>(
                `${environment.apiEndPoint}posts/${this.postId}`,
                {
                    headers: new HttpHeaders(
                        {
                            'Content-Type': 'application/json',
                        })
                }
            )
            .subscribe(Article => {
                    this.post = Article;
                    this.ionicForm.patchValue({title: this.post.title});
                    this.ionicForm.patchValue({intro: this.post.intro});
                    this.ionicForm.patchValue({url: this.post.url});
                    this.ionicForm.patchValue({body: this.post.body});
                    this.ionicForm.patchValue({visible: this.post.visible});
                    this.ionicForm.patchValue({location: this.post.location});
                    this.ionicForm.patchValue({publish: this.post.publish});
                    this.ionicForm.patchValue({category: this.post.category});
                    this.ionicForm.patchValue({image: this.post.image});
                    this.editor.setContents(this.post.body);
                },
                error => console.log(error)
            );
        console.log(this);
    }

    createForm() {
        this.ionicForm = new FormGroup({
            title: new FormControl(null, [Validators.required]),
            intro: new FormControl(null, [Validators.required]),
            url: new FormControl(null, [Validators.required]),
            body: new FormControl(null, [Validators.required]),
            visible: new FormControl(null, [Validators.required]),
            location: new FormControl(null, [Validators.required]),
            publish: new FormControl(null, [Validators.required]),
            category: new FormControl(null, [Validators.required]),
            image: new FormControl(null, [Validators.required])
        });
    }

    submitForm() {

    }
}
