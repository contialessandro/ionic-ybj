/*
* /*
import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MenuController, NavController, Platform} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';
import {Locations} from '../../../dataset/Locations';
import {Categories} from '../../../dataset/CategoryArticle';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {environment} from '../../../../environments/environment';
import {CrudService} from "../../../services/crud.service";
import {tap} from "rxjs/operators";
import {DomSanitizer,SafeHtml} from "@angular/platform-browser";
import {Article} from "../../../dataset/Article";
import Quill from 'quill';
import {ContentChange} from "ngx-quill";

@Component({
    selector: 'app-edit-post',
    templateUrl: './edit-post.page.html',
    styleUrls: ['./edit-post.page.scss'],
})
export class EditPostPage implements OnInit {
    categoryEndpoint = environment.apiEndPoint + 'categories/News';
    locationsEndpoint = environment.apiEndPoint + 'locations';
    getPostBaseEndpoint = environment.apiEndPoint + 'posts/';
    apiURL = environment.apiEndPoint;
    ionicForm: FormGroup;
    readonly editorStyle = { height: '15em' };

    public editor = Quill;
    locations: [Locations];
    categories: [Categories];
    postId: string;
    post: any;
    isLoading = false;
    private fileStoragePath: any;
    private slug: string;
    private file: File;
    private htmlSnippet: SafeHtml;
    public model = {
        editorData: '',
        config:{}
    };
    constructor(public platform: Platform,
                private splashScreen: SplashScreen,
                private statusBar: StatusBar,
                private http: HttpClient,
                private menuCtrl: MenuController,
                private router: Router,
                private route: ActivatedRoute,
                private crudService: CrudService,
                private navCtrl: NavController, private domSanitizer: DomSanitizer) {

    }
    onEditorCreated = (editor: Quill) => {
        this.editor = editor;
    };
    onContentChanged = (event: any) => {
        console.log(this.editor.root.innerHTML);
    }
    ngOnInit() {

        this.model.config =[

        ];
        this.postId = this.route.snapshot.params.postId;
        console.log(this.postId);
        this.route.paramMap.subscribe(
            paramMap => {
                if (!paramMap.has('postId')) {
                    this.navCtrl.navigateBack('/dashboard/home/posts/view');
                    return;
                }
                const postSub = this.http
                    .get(`${this.getPostBaseEndpoint}${this.postId}`)
                    .pipe(tap(res => {
                        console.log(res)
                    })).subscribe(
                        post => {
                            this.post = post;
                            this.LoadLocations();
                            this.LoadCategories();
                            this.formInit();
                            this.htmlSnippet = this.domSanitizer.bypassSecurityTrustHtml(this.post.body);
                            console.log(this.htmlSnippet);
                            this.model.editorData=this.post.body;
                            this.ionicForm.controls.freetext.patchValue(this.htmlSnippet);
                            this.ionicForm.controls.visible.patchValue(`${!this.post.hide}`);
                            this.PatchForm();

                            const location:any = this.LoadPostLocation(this.post.location);
                            this.ionicForm.controls.locations.patchValue(location.id);
                            console.log(location);
                        }
                    );
            }
        )

        // this.isLoading=true;
        // console.log(this);
        // this.crudService.GetPost(this.postId).subscribe(()=>{
        //     console.log(this);
        //     this.isLoading=false;
        // });
        // this.formInit();
        // this.PatchForm();
    }

    ionViewWillEnter() {

    }

    ionViewDidEnter() {

    }

    PatchForm() {
        this.ionicForm.controls.categories.patchValue(this.post.category);

        // this.ionicForm.controls.freetext.patchValue(this.post.body);
        // const editor =this.ionicForm.controls.editor;
        // const viewFragment = editor.data.processor.toView(this.post.body);
        // const modelFragment=editor.data.toModel(viewFragment);
    }

    formInit() {

        this.ionicForm=new FormGroup({
            title: new FormControl(this.post.title, [Validators.required]),
            intro: new FormControl(this.post.intro, [Validators.required]),
            url: new FormControl(this.post.url, [Validators.required]),
            body: new FormControl(null, [Validators.required]),
            // eslint-disable-next-line @typescript-eslint/naming-convention
            // eslint-disable-next-line @typescript-eslint/naming-convention
            delta: new FormControl(null),
            visible: new FormControl(this.post.visible, [Validators.required]),
            location: new FormControl(this.post.location, [Validators.required]),
            publish: new FormControl(this.post.published_at, [Validators.required]),
            category: new FormControl(this.post.category, [Validators.required]),
            image: new FormControl(this.post.image, [Validators.required])
        });
        console.log(this.ionicForm)
    }

    LoadLocations() {
        return this.http
            .get<[Locations]>(
                `${this.locationsEndpoint}/`,
                {
                    headers: new HttpHeaders(
                        {
                            'Content-Type': 'application/json',
                        })
                }
            )
            .subscribe(locations => {
                    this.locations=locations;
                },
                error => console.log(error)
            );
    }

    LoadCategories() {
        const api=`${this.categoryEndpoint}/`;
        return this.http.get<[Categories]>(api).subscribe(categories => {
                this.categories=categories;
            },
            error => console.log(error)
        );
    }

    LoadPostLocation(locationName){
        return this.http
            .get<Categories>(
                environment.apiEndPoint + `/location/${locationName}`).subscribe(location => {
                    return location
                },
                error => console.log(error)
            );
    }
    LoadPost() {
        this.crudService.GetPost(this.postId).subscribe();
    }

    // onFileChange(fileChangeEvent) {
    //     this.file = fileChangeEvent.target.files[0];
    // }
    //
    // SaveFile = async () => {
    //     (await this.GetCampaignSignedUrl({type: this.slug, file_name: this.file.name, file_type: this.file.type}))
    //         .subscribe(async (a: {
    //             signed_url: any, file_storage_path: any
    //         }) => {
    //             await this.UploadFile(a.signed_url);
    //             this.fileStoragePath = a.file_storage_path;
    //             const route = this.apiURL + `file/signedUrl`;
    //             return this.http.put(a.signed_url, this.file, {headers: {'Content-Type': this.file.type}});
    //         });
    // }
    //
    // UploadFile = async (signedUrl: string) => {
    //     return this.http.put(signedUrl, this.file, {headers: {'Content-Type': this.file.type}}).subscribe(a => {
    //     });
    // }
    //
    // GetCampaignSignedUrl = async (payload: any) => {
    //     const route = this.apiURL + `file/signedUrl`;
    //     return this.http.post<any>(route, payload);
    // }
    //
    // SavePost = async () => {
    //     const post: Article = {
    //         image: this.file.name,
    //         alt: this.file.name,
    //         body: this.ionicForm.value.freetext,
    //         category: this.ionicForm.value.category,
    //         color: '#ffeb00de',
    //         created_at: `${this.ionicForm.value.publish} 00:00:00`,
    //         published_at: `${this.ionicForm.value.publish} 00:00:00`,
    //         filename: this.file.name,
    //         id: 0,
    //         intro: this.ionicForm.value.intro,
    //         location: this.ionicForm.value.location,
    //         media_id: '0',
    //         slug: this.slug,
    //         hide: Boolean(+this.ionicForm.value.visible),
    //         ticker: '...',
    //         title: this.ionicForm.value.title,
    //         url: this.ionicForm.value.url,
    //         awsUrl: `${environment.s3BucketBaseUrl}/${this.fileStoragePath}`
    //     };
    //     console.log(post);
    //     const route = this.apiURL + `posts`;
    //     return this.http.post<any>(route, post).subscribe(a => {
    //         console.log(a);
    //     }, error => {
    //         console.log(error);
    //     });
    // }
    editorBody: any;

    onFileChange(fileChangeEvent) {
        this.file=fileChangeEvent.target.files[0];
    }

    async submitForm() {
        // console.log(this.file);
        // const payload: any = {
        //     thread: {
        //         title: this.ionicForm.value.title,
        //         intro: this.ionicForm.value.intro,
        //         body: this.ionicForm.value.freetext,
        //         published_at: this.ionicForm.value.publish,
        //         hide: this.ionicForm.value.visible,
        //         url: this.ionicForm.value.url,
        //         slug: 'asdad'
        //         /!*this.ionicForm.value.title.replace(/\s/g, '-').toLowerCase()*!/
        //     },
        //     thread_meta: {
        //         location_id: this.ionicForm.value.location,
        //         category_id: this.ionicForm.value.category,
        //         user_id: 1,
        //     },
        //     image_meta: {
        //         file_object: '', file_storage_path: ''
        //     }
        // };
        // this.slug = this.ionicForm.value.title.toLowerCase().replace(/\s/g, '-');
        // const res: any = await this.SaveFile();
        // await this.SavePost();
        // // console.log(this.file_storage_path);
        // const url = this.apiURL + 'file/upload';
        //
        // payload.file_object = this.file;
        // payload.image_meta.file_storage_path = this.fileStoragePath;
        //
        // return this.http.put(url, payload).subscribe((response) => {
        // });
    }
}
*/
