module.exports = {
    apps: [
        {
            name: "my-app",
            script: "./dist/www",
            env: {PORT: 3000, NODE_ENV: "production"}
        }
    ],
    deploy: {
        production: {
            user: "ubuntu",
            host: "ec2-18-130-140-138.eu-west-2.compute.amazonaws.com",
            key: "~/.ssh/apinodejs.pem",
            ref: "origin/master",
            repo: "git@bitbucket.org:contialessandro/ionic-ybj-api.git",
            path: "/home/ubuntu/my-app",
            "post-deploy": "npm install && pm2 startOrRestart ecosystem.config.js"
        }
    }
};
