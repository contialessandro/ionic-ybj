import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {EditPostPageRoutingModule} from './edit-post-routing.module';

import {EditPostPage} from './edit-post.page';
import {CustomQuillModule} from "../classes/quill-module";
import {QuillService} from "ngx-quill";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        EditPostPageRoutingModule,
        ReactiveFormsModule,
        CustomQuillModule
    ],
    exports: [
        EditPostPage
    ],
    declarations: [EditPostPage],
    providers: [QuillService]
})
export class EditPostPageModule {
}
