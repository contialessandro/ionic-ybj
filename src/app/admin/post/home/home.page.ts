import {Component, OnInit} from '@angular/core';
import {MenuController, Platform} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {HttpClient} from '@angular/common/http';
import {Route, Router} from '@angular/router';
import {CrudService} from '../../../services/crud.service';
import {Article} from '../../../dataset/Article';
import {Observable} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {finalize} from "rxjs/internal/operators/finalize";
import {ToastService} from "../../../services/toast.service";

@Component({
    selector: 'app-home',
    templateUrl: './home.page.html',
    styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

    dataIsLoaded = false;
    posts;
    cp = 1;
    rows: any;
    columns = [{prop: 'name'}, {name: 'Gender'}, {name: 'Company', sortable: false}];

    loadingIndicator = true;

    constructor(
        private http: HttpClient,
        private router: Router,
        private toastService: ToastService
        // private ionLoaderService: IonLoaderService
    ) {

        this.http
            .get<Article[]>(
                environment.apiEndPoint + `posts`)
            .pipe(finalize(() => {
                this.loadingIndicator = false;
                this.dataIsLoaded = true
            }))
            .subscribe((res) => {
                this.rows = res;
                console.log(this.rows);


            }, this.toastService.handleError);
    }


    redirectTo = (url: string, id: number = undefined) => (id) ? this.router.navigate([url, id]) : this.router.navigate([url]);

    ngOnInit() {
        // this.showLoader();

    }


}
