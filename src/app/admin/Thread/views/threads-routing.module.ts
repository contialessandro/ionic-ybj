import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomePage} from "./home/home.page";
import {AddPageModule} from "./add/add.module";

const routes: Routes = [
    {
        path: '',
        //loadChildren: () => import('./view/view.module').then( m => m.ViewPageModule)
        component: HomePage
    },
    {
        path: 'add',
        loadChildren: () => import('./add/add.module').then(m => m.AddPageModule)
    },
    {
        path: 'edit/:id',
        loadChildren: () => import('./edit/edit.module').then(m => m.EditPageModule)
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ThreadsRoutingModule {
}
