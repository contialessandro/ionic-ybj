import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AuthService } from '../services/auth.service';


@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
    signIn: FormGroup;

    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        public fb: FormBuilder,
        public authService: AuthService,
        private route: ActivatedRoute, private http: HttpClient
    ) {
        this.signIn=this.fb.group({
            email: ['contialessandroadmin@hotmail.it'],
            password: ['Lwe1f80Y81psnq']
        });
    }

    ngOnInit() {
    }

    loginUser() {
        this.authService.signIn(this.signIn.value);
    }

    getToken() {
        return localStorage.getItem('access_token');
    }

    setToken(token: string) {
        localStorage.setItem('access_token', token);
    }
}
