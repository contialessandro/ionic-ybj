import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';

@Injectable({
    providedIn: 'root'
})
export class IonLoaderService {
    constructor(
        public loadingController: LoadingController
    ) {
    }

    simpleLoader() {
        this.loadingController.create({
            message: ''
        }).then((response) => {
            response.present();
        });
    }

    dismissLoader() {
        this.loadingController.dismiss().then((response) => {
        }).catch((err) => {
            console.log('Error occured : ', err);
        });
    }

    autoLoader() {
        this.loadingController.create({
            message: 'Loader hides after 4 seconds',
            duration: 4000
        }).then((response) => {
            response.present();
            // tslint:disable-next-line:no-shadowed-variable
            response.onDidDismiss().then((response) => {
            });
        });
    }

    customLoader() {
        this.loadingController.create({
            message: 'Loader with custom style',
            duration: 4000,
            cssClass: 'loader-css-class',
            backdropDismiss: true
        }).then((res) => {
            res.present();
        });
    }
}
