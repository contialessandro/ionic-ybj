import {Injectable} from '@angular/core';
import {Observable, throwError} from 'rxjs';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';
import {Article} from '../dataset/Article';
import {environment} from '../../environments/environment';


@Injectable({
    providedIn: 'root'
})

export class CrudService {
    categoryEndpoint = environment.apiEndPoint + 'categories/News';
    locationsEndpoint = environment.apiEndPoint + 'locations';
    headers = new HttpHeaders().set('Content-Type', 'application/json');

    constructor(
        private http: HttpClient,
        public router: Router
    ) {
    }

    // Categories
    getCategories(): Observable<any> {
        const api = `${this.categoryEndpoint}/`;
        return this.http.get(api);
    }

    // Locations
    getLocations(): Observable<any> {
        const api = `${this.locationsEndpoint}/`;
        return this.http.get(api);
    }

    getLocationByName(): Observable<any> {
        const api = `${this.locationsEndpoint}/`;
        return this.http.get(api);
    }

    // posts views
    getAllPosts(): Observable<any> {
        const api = environment.apiEndPoint + `posts`;
        return this.http.get(api);
    }

    getPost(postId: string): Observable<Article> {
        console.log(this.http
            .get<Article>(
                environment.apiEndPoint + `posts/` + postId));
        return this.http
            .get<Article>(
                environment.apiEndPoint + `posts/` + postId);
    }

    GetAccountInfo(): Observable<any> {
        return this.http.get(environment.apiEndPoint + `posts`);
    }

    // Error
    handleError(error: HttpErrorResponse) {
        let msg;
        if (error.error instanceof ErrorEvent) {
            // client-side error
            msg = error.error.message;
        } else {
            // server-side error
            msg = `Error Code: ${error.status}\nMessage: ${error.message}`;
        }
        return throwError(msg ?? '');
    }
}
