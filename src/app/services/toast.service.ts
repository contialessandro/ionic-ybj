import {Injectable} from '@angular/core';
import {ToastController} from '@ionic/angular';

export type ToastPosition = | 'top' | 'bottom' | 'middle';

@Injectable({
    providedIn: 'root'
})
export class ToastService {
    private toast: HTMLIonToastElement;

    constructor(private toastController: ToastController) {
        this.toast = null;
    }

    async showError(err: any, duration: number = -1, color: string = 'danger', position: ToastPosition = 'bottom') {
        const header = (err.error.title) ? err.error.title : err.statusText;
        const message = (err.error.message) ? err.error.message : err.message;
        await this.show(message, header, duration, color, position);
    }

    async show(message: string, header: string, duration: number = -1, color: string = 'danger', position: ToastPosition = 'bottom') {
        this.hide();
        this.toast = await this.toastController.create({
            header,
            color,
            message,
            duration,
            position,
            buttons: [
                {
                    text: 'OK',
                    role: 'cancel'
                }
            ]
        });
        return await this.toast.present();
    }

    async hide() {
        try {
            if (this.toast !== null) {
                this.toast.dismiss();
                this.toast = null;
            }
        } catch (e) {
        }
    }

    public handleError = (err: any) => this.showError(err, 4000);
}
