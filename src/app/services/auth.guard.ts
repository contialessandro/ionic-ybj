import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';


@Injectable({
    providedIn: 'root'
})

export class AuthGuard implements CanActivate {
    private readonly Roles
        =new Map<DashboardRoles, (router: Router) => Promise<boolean | UrlTree>>([
        [DashboardRoles.Allowed, () => Promise.resolve(true)],
        [DashboardRoles.Denied, () => Promise.resolve(this.NavigateToLogin())],
    ]);

    constructor(public authService: AuthService, public router: Router) {
    }

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        if (this.authService.isLoggedIn !== true) {
            window.alert('Access not allowed!');
            this.router.navigate(['login']);
        }
        return true;
    }

    private NavigateToLogin(): UrlTree {
        return this.router.parseUrl('/login');
    }

}

export enum DashboardRoles {
    Allowed=1,
    Denied=2,
}
