import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { User } from '../dataset/user';
import { environment } from '../../environments/environment';

@Injectable({
    providedIn: 'root'
})

export class AuthService {
    endpoint = environment.apiEndPoint + 'users';
    headers = new HttpHeaders().set('Content-Type', 'application/json');
    currentUser={};

    constructor(
        private http: HttpClient,
        public router: Router
    ) {
    }

    get isLoggedIn(): boolean {
        return (localStorage.getItem('access_token') !== null);
    }

    // Sign-up
    signUp(user: User): Observable<any> {
        const api=`${this.endpoint}/register-user`;
        return this.http.post(api, user)
            .pipe(
                catchError(this.handleError)
            );
    }

    getToken() {
        return localStorage.getItem('access_token');
    }

    // Sign-in
    signIn(user: User) {
        return this.http.post<any>(`${this.endpoint}/signin`, user)
            .subscribe((res: any) => {
                if (res.token) {
                    localStorage.setItem('access_token', res.token);
                    console.log('white smoke');
                    this.router.navigate(['/admin/posts/home']);
                }
                // tslint:disable-next-line:no-shadowed-variable
                // this.getUserProfile(res._id).subscribe((res) => {
                //     this.currentUser = res;
                //
                // });
            });
    }

    doLogout() {
        localStorage.removeItem('access_token');
        if (localStorage.getItem('access_token') === null) {
            this.router.navigate(['login']);
        }
    }

    // User profile
    getUserProfile(id): Observable<any> {
        const api=`${this.endpoint}/user-profile/${id}`;
        return this.http.get(api, {headers: this.headers}).pipe(
            map((res: Response) => {
                return res || {};
            }),
            catchError(this.handleError)
        );
    }

    // Error
    handleError(error: HttpErrorResponse) {
        let msg;
        if (error.error instanceof ErrorEvent) {
            // client-side error
            msg=error.error.message;
        } else {
            // server-side error
            msg=`Error Code: ${error.status}\nMessage: ${error.message}`;
        }
        return throwError(msg ?? '');
    }
}
