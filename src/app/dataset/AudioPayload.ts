export interface AudioRequestInterface {
    text: string,
    filename: string,
    post_id: number
}