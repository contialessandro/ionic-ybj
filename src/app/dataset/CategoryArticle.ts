export interface CategoryArticle {
    image: string;
    id: number;
    title: string;
    intro: string;
    body: string;
    location: string;
    ticker: string;
    color: string;
    alt: string;
    category: string;
    filename: string;
    media_id: number;
    published_at: string;
    slug: string;
    awsUrl: string;
    hide: boolean;
}

export interface Categories {
    color: string;
    createdAt: string;
    id: string;
    name: string;
    type: string;
    updatedAt: string;
    __v: number;
    _id: string;
}
