export interface Locations {
    createdAt: string;
    id: number;
    name: string;
    updatedAt: string;
    __v: number;
    _id: string;
}
