export interface Thread {
    _id?: string;
    title: string;
    intro: string;
    body: string;
    published_at: any;
    hide: string;
    url: string;
    slug: string;
    meta_id?: string;
}