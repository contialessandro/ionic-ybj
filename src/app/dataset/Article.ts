export interface Article {
    title: string;
    intro: string;
    url: string;
    body: string;
    hide?: boolean;
    location: string;
    published_at: string | Date;
    image: string;
    category: string;
    alt: string;
    color?: string;
    awsUrl?: string;
    created_at: string;
    filename: string;
    id?: number;
    media_id?: string;
    slug: string;
    ticker: string;
    thread_meta_id?: string;
    audio_url?: string;
    _id?: string;
}
