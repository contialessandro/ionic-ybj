export interface ArticleHomepage
{
    image: string;
    id: number;
    title: string;
    intro: string;
    body: string;
    location: string;
    ticker: string;
    color: string;
    alt: string;
    category: string;
    filename: string;
    media_id: number;
    published_at: string;
    slug: string;
    awsUrl: string;
}

