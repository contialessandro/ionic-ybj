export interface ThreadMeta {
    _id?: string;
    image_id?: string;
    location_id: string;
    category_id: string;
    thread_id: 1;
    user_id: string;
}