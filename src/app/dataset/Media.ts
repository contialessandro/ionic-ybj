export interface IMedia {
    _id?: string,
    model_id: string,
    name: string,
    file_name: string,
    mime_type: string
}