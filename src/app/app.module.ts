import {CUSTOM_ELEMENTS_SCHEMA, ErrorHandler, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouteReuseStrategy} from '@angular/router';

// Import modules
import {IonicModule, IonicRouteStrategy, Platform} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';

import {HttpClientModule} from '@angular/common/http';
import {MenuComponentModule} from './menu/menu.module';
import {ArticleCardModule} from './home/article-card/article-card.module';
import {HeaderTitlePageModule} from './headerTitle/header-title.module';
import {HtmlsanificationPipe} from './htmlsanification.pipe';
import {ShareButtonsModule} from 'ngx-sharebuttons/buttons';
import {ShareIconsModule} from 'ngx-sharebuttons/icons';
import {FileTransfer} from '@ionic-native/file-transfer/ngx';
import {GlobalErrorHandler} from './services/globalErrorHandler';
import {PostCrudRoutingModule} from "./admin/post/post-crud-routing.module";
import {ThreadsRoutingModule} from "./admin/Thread/views/threads-routing.module";
import {NgxPaginationModule} from "ngx-pagination";

@NgModule({
    declarations: [AppComponent, HtmlsanificationPipe],
    entryComponents: [],
    imports: [
        BrowserModule,
        IonicModule.forRoot(),
        AppRoutingModule,
        HttpClientModule,
        MenuComponentModule,
        ArticleCardModule,
        HeaderTitlePageModule,
        ShareButtonsModule,
        ShareIconsModule, ThreadsRoutingModule,
        NgxPaginationModule
    ],
    providers: [
        Platform,
        StatusBar,
        SplashScreen,
        {provide: RouteReuseStrategy, useClass: IonicRouteStrategy},
        FileTransfer,
        {provide: ErrorHandler, useClass: GlobalErrorHandler}
    ],
    bootstrap: [AppComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})

export class AppModule {
}
