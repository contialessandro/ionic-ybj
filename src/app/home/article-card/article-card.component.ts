import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { environment } from '../../../environments/environment';
import { ArticleHomepage } from '../../dataset/articleHomepage.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-article-card',
    templateUrl: './article-card.component.html',
    styleUrls: ['./article-card.component.scss'],
})
export class ArticleCardComponent implements OnInit {

    homepageArticles: ArticleHomepage[];
    homepageArticle: ArticleHomepage;
    final=[];
    public isDataAvailable=false;
    assetUrl=environment.s3BucketBaseUrl;
    HOMEPAGEARTICLESCOUNT=9;

    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        private route: ActivatedRoute, private http: HttpClient
    ) {
        this.initializeApp();
    }

    ngOnInit(): void {
        this.http
            .get<[ArticleHomepage]>(
                environment.apiEndPoint + 'HomePagePosts',
                {
                    headers: new HttpHeaders(
                        {
                            'Content-Type': 'application/json',
                        })
                }
            )
            .subscribe(homepageArticles => {
                    this.final=this.chunk(homepageArticles, 3);
                    this.homepageArticles=homepageArticles;
                    this.homepageArticles.forEach((article) => {
                        article.intro=article.intro.toLowerCase();
                        if (article.intro.length > 95) {
                            article.intro=article.intro.slice(0, 90) + '...';
                        }
                    });
                    this.isDataAvailable=true;
                },
                error => console.log(error)
            );
    }

    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleDefault();
            this.splashScreen.hide();
        });
    }

    chunk(arr, len) {
        const chunks=[];
        let i=0;
        while (i < this.HOMEPAGEARTICLESCOUNT) {

            chunks.push(arr.slice(i, i+=len));
        }
        return chunks;
    }

}
