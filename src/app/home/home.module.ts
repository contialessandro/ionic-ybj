import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {HomePageRoutingModule} from './home-routing.module';

import {HomePage} from './home.page';
import {ArticleCardModule} from './article-card/article-card.module';
import {HeaderTitlePageModule} from '../headerTitle/header-title.module';
import {CKEditorModule} from '@ckeditor/ckeditor5-angular';

// import { IonicPageModule } from '@ionic/angular';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomePageRoutingModule,
    ArticleCardModule,
    HeaderTitlePageModule,
    CKEditorModule,
    // IonicPageModule.forChild(HomePage)
  ],
  declarations: [HomePage], schemas: [CUSTOM_ELEMENTS_SCHEMA]

})
export class HomePageModule {}
