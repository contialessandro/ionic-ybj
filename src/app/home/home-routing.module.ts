import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomePage } from './home.page';
import {CategoryPage} from '../category/category.page';
import {ArticlePage} from '../article/article.page';

const routes: Routes = [
  {
    path: '',
    component: HomePage
  },
  {
      path: 'topic/:category',
      component: CategoryPage
  },
  {
    path: 'threads/:articleSlug',
    component: ArticlePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomePageRoutingModule {}
