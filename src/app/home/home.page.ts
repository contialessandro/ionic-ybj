import {Component, OnInit} from '@angular/core';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
// import { IonicPage } from '@ionic/angular';
//
// @IonicPage()
@Component({
    selector: 'app-home',
    templateUrl: './home.page.html',
    styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
    rootPage: any = 'HomePage';

    constructor() {
    }

    public Editor = ClassicEditor;

    ngOnInit(): void {
    }
}
