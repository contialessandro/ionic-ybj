import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { CategoryArticle } from '../dataset/CategoryArticle';

@Component({
    selector: 'app-category',
    templateUrl: './category.page.html',
    styleUrls: ['./category.page.scss'],
})
export class CategoryPage implements OnInit {
    assetUrl=environment.s3BucketBaseUrl;
    categoryArticles: [CategoryArticle];
    category='';
    localPath='../../../assets/img/cropped-TRANS-LARGE-1.jpeg';

    constructor(private route: ActivatedRoute, private http: HttpClient) {
        const category='';
    }

    ngOnInit(): void {

        this.category=this.route.snapshot.params.category;

        this.http
            .get<[CategoryArticle]>(
                environment.apiEndPoint + 'thread/topic/' + this.category,
                {
                    headers: new HttpHeaders(
                        {
                            'Content-Type': 'application/json'
                        })
                }
            )
            .subscribe(categoryArticles => {

                    this.categoryArticles=categoryArticles;
                    // this.categoryArticles.forEach(article => (
                    //     article.awsUrl =
                    //         article.id === 73 ? this.localPath : article.image
                    // ));
                },
                error => console.log(error.message)
            );
    }
}
