export const environment={
    production: true,
    s3BucketBaseUrl: 'https://ybjlaravel.s3.eu-west-2.amazonaws.com',
    apiEndPoint: 'https://api.yorkshirebusinessjournal.co.uk/'
};
