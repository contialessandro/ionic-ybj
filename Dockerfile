## Build
FROM node:12.18.2-alpine3.11 as build

RUN apk add --no-cache python3

RUN npm install -g @ionic/cli

RUN npm install -g @angular/cli

# Create app directory
WORKDIR /usr/src/app
# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package.json .
RUN npm i
# Bundle app source
COPY . .
RUN ionic build --prod

## Run 
FROM nginx:1.19.0-alpine as prod

# Set Timezone
ARG TZ='Europe/London'
ENV DEFAULT_TZ ${TZ}
RUN cp /usr/share/zoneinfo/${DEFAULT_TZ} /etc/localtime

# COPY www /usr/share/nginx/html
COPY --from=build  /usr/src/app/www /var/www/
COPY default.conf /etc/nginx/conf.d/default.conf
